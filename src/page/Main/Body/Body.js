import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import TaiKhoan from '../../../views/Main/QuanLy/TaiKhoan'
function Body() {
    return (
       <Switch>
            <Route path="/TaiKhoan" render={()=><TaiKhoan></TaiKhoan>}></Route>
            <Route path="/" render={()=><div style={{height:'100vh'}}></div>}></Route>
       </Switch> 
    )
}

export default Body
