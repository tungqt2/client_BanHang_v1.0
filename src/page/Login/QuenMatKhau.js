import React from 'react'
// Quên mật khẩu khi click trở về truyền thao tác lên phần form 
// thực hiện setState lại setOnSwitchForm(false)

// Quên mật khẩu đăng nhập đúng tài khoản có trong hệ thống, hệ thống
// sẽ gửi reset mã trên gmail
const QuenMatKhau = ({onClickTroVe})=>{
    return(
        <div className="login-page">
        <div className="login-box">
            <div className="login-logo">
                <a href=""><b>Quản lý </b>bán hàng</a>
            </div>
            <div className="card">
                <div className="card-body login-card-body">
                    <p className="login-box-msg">Quên mật khẩu</p>
                    <div>
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder="Điền tài khoản người dùng" />
                            <div className="input-group-append">
                                <div className="input-group-text">
                                    <span className="fas fa-user" />
                                </div>
                            </div>
                        </div>
                        {/* <div className="input-group mb-3">
                            <input type="password" className="form-control" placeholder="Mật khẩu" />
                            <div className="input-group-append">
                                <div className="input-group-text">
                                    <span className="fas fa-lock" />
                                </div>
                            </div>
                        </div> */}
                        <div className="row">
                            <div className="col-12">
                                {/* <label>Điền tài khoản người dùng</label> */}
                                <label>Mật khẩu mới gửi về tài khoản đã đăng ký gmail</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-8">
                            </div>
                            <div className="col-12">
                                <button type="submit" className="btn btn-primary btn-block">Gửi</button>
                            </div>
                        </div>
                    </div>

                    <p className="mb-1 mt-2">
                        <a style={{ float: 'right', cursor: 'pointer', color: '#007bff' }} 
                        onClick={()=> onClickTroVe(true)}
                        >Trở về</a>
                    </p>
                    <p className="mb-0">
                        <a href="register.html" className="text-center"></a>
                    </p>
                </div>
            </div>
        </div>

    </div>
    )
}


export default QuenMatKhau
