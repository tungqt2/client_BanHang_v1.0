import React from 'react'
import DSTaiKhoan from '../../../compontents/Main/QuanLy/TaiKhoan/DSTaiKhoan'
import ThemTaiKhoan from '../../../compontents/Main/QuanLy/TaiKhoan/ThemTaiKhoan'

function TaiKhoan() {
    return (
        <div className="content-wrapper">
            <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                <div className="col-sm-6">
                    <h1>Tài khoản</h1>
                </div>
                <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right">
                    <li className="breadcrumb-item"><a href="#">Trang chủ</a></li>
                    <li className="breadcrumb-item active">Tài khoản</li>
                    </ol>
                </div>
                </div>
            </div>
            </section>
            <div className="content">
                <div className="container-fluid">
                    <ThemTaiKhoan></ThemTaiKhoan>
                    <DSTaiKhoan></DSTaiKhoan>   
                </div>
            </div>
        </div>
    )
}

export default TaiKhoan
