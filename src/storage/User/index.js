import func from "../../asset/func"

const setUser = (data)=>{
    window.localStorage.setItem(func.Encode_LoopBtoa(10,'User'), data)
}
const getUser = ()=>{
    return window.localStorage.getItem(func.Encode_LoopBtoa(10,'User')) === null ? [] :
    func.DecodeJson(window.localStorage.getItem(func.Encode_LoopBtoa(10,'User')))
}

export default {
    setUser,getUser
}